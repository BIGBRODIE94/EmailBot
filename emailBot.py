import smtplib
import speech_recognition as sr
import pyttsx3
from email.message import EmailMessage

listener = sr.Recognizer()
engine = pyttsx3.init()


def talk(text):
    engine.say(text)
    engine.runAndWait()


def get_info():
    try:
        with sr.Microphone() as source:
            print('Listening....')
            voice = listener.listen(source)
            info = listener.recognize_google(voice)
            print(info)
            return info.lower()
    except:
        pass


def send_email(receiver, subject, message):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login('Your Email', 'Your Password')
    email = EmailMessage()
    email['From'] = 'Your Email'
    email['To'] = receiver
    email['Subject'] = subject
    email.set_content(message)
    server.send_message(email)
    


email_list = {
    Friend1 = 'Friend1@gmail.com'
    Friend2 = 'Friend2@gmai.com'
    Friend3 = 'Friend3@gmail.com'
}


def get_email_info():
    talk('Hello Sir! Who do you like to send email?')
    name = get_info()
    receiver = email_list[name]
    print(receiver)
    talk('What is the title of your email sir?')
    subject = get_info()
    talk('What is the description of your email sir?')
    message = get_info()
    send_email(receiver, subject, message)
    talk('Your email is sent sir')
    talk('Would you like to send more emails Sir?')
    send_more = get_info()
    if 'yes' in send_more:
        get_email_info()


get_email_info()
